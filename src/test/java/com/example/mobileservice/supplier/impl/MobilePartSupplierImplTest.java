package com.example.mobileservice.supplier.impl;

import com.example.mobileservice.mobile.Manufacturer;
import com.example.mobileservice.mobile.MobilePart;
import com.example.mobileservice.mobile.MobilePartType;
import com.example.mobileservice.mobile.MobilePartUnit;
import com.example.mobileservice.helper.IdGenerator;
import com.example.mobileservice.helper.SerialNumberGenerator;
import com.example.mobileservice.helper.TaskScheduler;
import com.example.mobileservice.listener.StatusChangeListener;
import com.example.mobileservice.supplier.Order;
import com.example.mobileservice.supplier.OrderStatus;
import com.example.mobileservice.supplier.OrderStatusChangedEvent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

class MobilePartSupplierImplTest {

    @InjectMocks
    private MobilePartSupplierImpl supplier;

    @Mock
    private IdGenerator idGenerator;

    @Mock
    private SerialNumberGenerator serialNumberGenerator;

    @Mock
    private TaskScheduler taskScheduler;

    @Mock
    private StatusChangeListener<OrderStatusChangedEvent>statusChangeListener;

    @BeforeEach
    void beforeEach() {
        MockitoAnnotations.initMocks(this);
    }

    @Nested
    @DisplayName("When orderPart is called with valid parameters")
    class OrderPartWithValidData {
        MobilePartType partType;
        int quantity;
        Order returnedOrder;
        long generatedId = 345;

        @BeforeEach
        void beforeEach() {
            partType = new MobilePartType(Manufacturer.APPLE, MobilePartUnit.DISPLAY, "iPhone 6 display");
            quantity = 3;
            given(idGenerator.generateId())
                    .willReturn(generatedId);


            returnedOrder = supplier.orderPart(partType, quantity, statusChangeListener);
        }

        @Test
        @DisplayName("Returned Order should not be null")
        void orderPart() {
            assertNotNull(returnedOrder);
        }

        @Test
        @DisplayName("Returned Order.id = generated id")
        void generatedId() {
            assertEquals(generatedId, returnedOrder.getId());
        }

        @Test
        @DisplayName("Returned Order.partType equals to sent type")
        void type() {
            assertEquals(partType, returnedOrder.getPartType());
        }
        @Test
        @DisplayName("Returned Order.status equals to ORDERED")
        void status() {
            assertEquals(OrderStatus.ORDERED, returnedOrder.getStatus());
        }

        @Test
        @DisplayName("taskScheduler.scheduleTaskToRandomTime is invoked")
        void taskScheduled() {
            verify(taskScheduler).scheduleTaskToRandomTime(any(Runnable.class), anyLong(), anyLong(), any(TimeUnit.class));
        }

        @Test
        @DisplayName("Calling Order.ship() throws an IllegalStateException")
        void ship() {
            assertThrows(IllegalStateException.class, () -> returnedOrder.ship());
        }

        @Nested
        @DisplayName("When timed task is scheduled")
        class WhenTaskIsScheduled {
            private Runnable taskSentToScheduler;
            @BeforeEach
            void beforeEach() {
                final ArgumentCaptor<Runnable> taskCaptor = ArgumentCaptor.forClass(Runnable.class);
                verify(taskScheduler).scheduleTaskToRandomTime(taskCaptor.capture(), anyLong(), anyLong(), any(TimeUnit.class));
                taskSentToScheduler = taskCaptor.getValue();

                //executing task:
                taskSentToScheduler.run();
            }

            @Test
            @DisplayName("order's status should be set to READY_FOR_SHIPMENT")
            void checkOrderStatus() {
                assertEquals(OrderStatus.READY_FOR_SHIPMENT, returnedOrder.getStatus());
            }

            @Test
            @DisplayName("registered statusChangeListener's statusChanged called")
            void listenerCalled() {
                verify(statusChangeListener).statusChanged(any(OrderStatusChangedEvent.class));
            }

            @Test
            @DisplayName("Calling Order.ship() returns the manufactured MobileParts")
            void ship() {
                final List<MobilePart> shippedParts = returnedOrder.ship();
                assertEquals(quantity, shippedParts.size());
                assertEquals(partType, shippedParts.get(0).type);
            }

        }
    }
}