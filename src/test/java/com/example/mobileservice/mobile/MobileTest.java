package com.example.mobileservice.mobile;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class MobileTest {

    private Manufacturer manufacturer;
    private String model;

    @BeforeEach
    void beforeEach() {
        manufacturer = Manufacturer.SAMSUNG;
        model = "S9";
    }

    @Test
    @DisplayName("Manufacturer should be provided")
    void manufacturerShouldNotBeEmpty(){
        assertThrows(IllegalArgumentException.class, () -> new Mobile.Builder(null, model));
    }

    @Test
    @DisplayName("Model should be provided")
    void modelShouldNotBeEmpty(){
        assertThrows(IllegalArgumentException.class, () -> new Mobile.Builder(manufacturer, null));
    }

    @Nested
    @DisplayName("Mobile should have all the correct attributes if it is built by the Builder")
    class BuilderTest {

        private Mobile.Builder builder;

        @BeforeEach
        void beforeEach(){
            builder = new Mobile.Builder(manufacturer, model);
        }

        @Test
        @DisplayName("Manufacturer should have the correct value")
        void shouldSetTheCorrectManufacturer(){
            final Mobile mobile = builder.build();
            assertEquals(mobile.getManufacturer(), manufacturer);
        }

        @Test
        @DisplayName("Model should have the correct value")
        void shouldSetTheCorrectModel(){
            final Mobile mobile = builder.build();
            assertEquals(mobile.getModel(), model);
        }

        @Test
        @DisplayName("Mobile should have the correct otherInfo")
        void shouldSetTheCorrectOtherInfo(){
            final String otherInfo = "rounded glass screen";
            builder.otherInfo(otherInfo);
            final Mobile mobile = builder.build();

            assertEquals(otherInfo, mobile.getOtherInfo());

        }

        @Nested
        @DisplayName("MobileParts build")
        class TestMobilePartsBuild {

            private MobilePart mobilePart;


            @BeforeEach
            void beforeEach(){
                mobilePart = mock(MobilePart.class);
            }

            @Test
            @DisplayName("Mobile should have the correct display")
            void shouldSetTheCorrectDisplay() {

                builder.display(mobilePart);
                final Mobile mobile = builder.build();

                assertEquals(mobilePart, mobile.getDisplay());
            }

            @Test
            @DisplayName("Mobile should have the correct Motherboard")
            void shouldSetTheCorrectMotherBoard() {
                //when
                builder.motherBoard(mobilePart);
                final Mobile mobile = builder.build();

                //then
                assertEquals(mobilePart, mobile.getMotherBoard());
            }

            @Test
            @DisplayName("Mobile should have the correct Keyboard")
            void shouldSetTheCorrectKeyBoard() {
                //when
                builder.keyboard(mobilePart);
                final Mobile mobile = builder.build();

                //then
                assertEquals(mobilePart, mobile.getKeyboard());
            }

            @Test
            @DisplayName("Mobile should have the correct Microphone")
            void shouldSetTheCorrectMicrophone() {
                //when
                builder.microphone(mobilePart);
                final Mobile mobile = builder.build();

                //then
                assertEquals(mobilePart, mobile.getMicrophone());
            }

            @Test
            @DisplayName("Mobile should have the correct Speaker")
            void shouldSetTheCorrectSpeaker() {
                //when
                builder.speaker(mobilePart);
                final Mobile mobile = builder.build();

                //then
                assertEquals(mobilePart, mobile.getSpeaker());
            }

            @Test
            @DisplayName("Mobile should have the correct Volume Buttons")
            void shouldSetTheCorrectVolumeButtons() {
                //when
                builder.volumeButtons(mobilePart);
                final Mobile mobile = builder.build();

                //then
                assertEquals(mobilePart, mobile.getVolumeButtons());
            }

            @Test
            @DisplayName("Mobile should have the correct Power Switch")
            void shouldSetTheCorrectPowerSwitch() {
                //when
                builder.powerSwitch(mobilePart);
                final Mobile mobile = builder.build();

                //then
                assertEquals(mobilePart, mobile.getPowerSwitch());
            }
        }

    }
}