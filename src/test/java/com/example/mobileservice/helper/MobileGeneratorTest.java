package com.example.mobileservice.helper;

import com.example.mobileservice.mobile.Mobile;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MobileGeneratorTest {

    private MobileGenerator mobileGenerator;
    private Mobile mobile;

    @BeforeEach
    public void beforeEach(){
        mobileGenerator = new MobileGenerator();
    }

    @Test
    public void generateMobileTest() {
        mobile = mobileGenerator.generateMobile();

        assertNotNull(mobile);
    }
}