package com.example.mobileservice.helper;

import com.example.mobileservice.mobile.Mobile;

import java.util.Random;

public class RandomGeneratorImp implements RandomGenerator {
    private Random random;

    public RandomGeneratorImp() {
        this.random = new Random();
    }

    public int generateRandomPositiveInt(int upperRange){
        return random.nextInt(upperRange);
    }

    public boolean generateRandomBoolean(){
        return random.nextBoolean();
    }

}
