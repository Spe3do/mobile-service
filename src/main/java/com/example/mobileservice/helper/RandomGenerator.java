package com.example.mobileservice.helper;

import com.example.mobileservice.mobile.Mobile;

public interface RandomGenerator {

    int generateRandomPositiveInt(int upperRange);
    boolean generateRandomBoolean();

}
