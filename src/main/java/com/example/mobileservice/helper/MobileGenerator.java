package com.example.mobileservice.helper;

import com.example.mobileservice.mobile.*;

public class MobileGenerator {

    private SerialNumberGenerator serialNumberGenerator;
    private RandomGenerator randomGenerator;

    private static final Manufacturer[] manufacturers = Manufacturer.values();
    private static final String[] samsungModels = {"Galaxy S3", "Galaxy S4", "Galaxy S5", "Galaxy S6", "Galaxy S7", "Galaxy Note"};
    private static final String[] htcModels = {"One", "Desire 610", "One Mini"};
    private static final String[] huaweiModels = {"Ascend P7", "Ascend P2", "Ascend G630"};
    private static final String[] appleModels = {"iPhone 4s", "iPhone 5s", "iPhone5c", "iPhone 6", "iPhone 6s", "iPhone 7", "iPhone 8", "iPhone X"};

    public MobileGenerator() {
        serialNumberGenerator = new IncrementalSerialNumberGenerator();
        randomGenerator = new RandomGeneratorImp();
    }

    public Mobile generateMobile(){
        return createRandomPhone();
    }

    private Mobile createRandomPhone() {
        Manufacturer m = manufacturers[randomGenerator.generateRandomPositiveInt(manufacturers.length)];
        switch (m) {
            case APPLE:
                return createApplePhone();
            case HTC:
                return createHTCPhone();
            case HUAWEI:
                return createHuaweiPhone();
            case SAMSUNG:
                return createSamsungPhone();
            default:
                throw new AssertionError("Unexpected manufacturer: " + m);
        }
    }

    private Mobile createSamsungPhone() {
        Mobile mobile;
        Manufacturer manufacturer = Manufacturer.SAMSUNG;
        String model = samsungModels[randomGenerator.generateRandomPositiveInt(samsungModels.length)];

        mobile = new Mobile.Builder(manufacturer, model)
                .display(new MobilePart(new MobilePartType(manufacturer, MobilePartUnit.DISPLAY, model + " Display"), randomGenerator.generateRandomBoolean(), serialNumberGenerator.generateSerialNumber()))
                .microphone(new MobilePart(new MobilePartType(manufacturer, MobilePartUnit.MICROPHONE, model + " Microphone"), randomGenerator.generateRandomBoolean(), serialNumberGenerator.generateSerialNumber()))
                .motherBoard(new MobilePart(new MobilePartType(manufacturer, MobilePartUnit.MOTHERBOARD, model + " MotherBoard"), randomGenerator.generateRandomBoolean(), serialNumberGenerator.generateSerialNumber()))
                .powerSwitch(new MobilePart(new MobilePartType(manufacturer, MobilePartUnit.POWER_SWITCH, model + " Power Switch"), randomGenerator.generateRandomBoolean(), serialNumberGenerator.generateSerialNumber()))
                .speaker(new MobilePart(new MobilePartType(manufacturer, MobilePartUnit.SPEAKER, model + " Speaker"), randomGenerator.generateRandomBoolean(), serialNumberGenerator.generateSerialNumber()))
                .microphone(new MobilePart(new MobilePartType(manufacturer, MobilePartUnit.MICROPHONE, model + " Microphone"), randomGenerator.generateRandomBoolean(), serialNumberGenerator.generateSerialNumber()))
                .volumeButtons(new MobilePart(new MobilePartType(manufacturer, MobilePartUnit.VOLUME_BUTTONS, model + " Volume Buttons"), randomGenerator.generateRandomBoolean(), serialNumberGenerator.generateSerialNumber()))
                .build();

        return  mobile;
    }

    private Mobile createApplePhone() {
        Mobile mobile;
        Manufacturer manufacturer = Manufacturer.APPLE;
        String model = appleModels[randomGenerator.generateRandomPositiveInt(appleModels.length)];

        mobile = new Mobile.Builder(manufacturer, model)
                .display(new MobilePart(new MobilePartType(manufacturer, MobilePartUnit.DISPLAY, model + " Display"), randomGenerator.generateRandomBoolean(), serialNumberGenerator.generateSerialNumber()))
                .microphone(new MobilePart(new MobilePartType(manufacturer, MobilePartUnit.MICROPHONE, model + " Microphone"), randomGenerator.generateRandomBoolean(), serialNumberGenerator.generateSerialNumber()))
                .motherBoard(new MobilePart(new MobilePartType(manufacturer, MobilePartUnit.MOTHERBOARD, model + " MotherBoard"), randomGenerator.generateRandomBoolean(), serialNumberGenerator.generateSerialNumber()))
                .powerSwitch(new MobilePart(new MobilePartType(manufacturer, MobilePartUnit.POWER_SWITCH, model + " Power Switch"), randomGenerator.generateRandomBoolean(), serialNumberGenerator.generateSerialNumber()))
                .speaker(new MobilePart(new MobilePartType(manufacturer, MobilePartUnit.SPEAKER, model + " Speaker"), randomGenerator.generateRandomBoolean(), serialNumberGenerator.generateSerialNumber()))
                .microphone(new MobilePart(new MobilePartType(manufacturer, MobilePartUnit.MICROPHONE, model + " Microphone"), randomGenerator.generateRandomBoolean(), serialNumberGenerator.generateSerialNumber()))
                .volumeButtons(new MobilePart(new MobilePartType(manufacturer, MobilePartUnit.VOLUME_BUTTONS, model + " Volume Buttons"), randomGenerator.generateRandomBoolean(), serialNumberGenerator.generateSerialNumber()))
                .build();

        return  mobile;
    }

    private Mobile createHTCPhone() {
        Mobile mobile;
        Manufacturer manufacturer = Manufacturer.HTC;
        String model = htcModels[randomGenerator.generateRandomPositiveInt(htcModels.length)];

        mobile = new Mobile.Builder(manufacturer, model)
                .display(new MobilePart(new MobilePartType(manufacturer, MobilePartUnit.DISPLAY, model + " Display"), randomGenerator.generateRandomBoolean(), serialNumberGenerator.generateSerialNumber()))
                .microphone(new MobilePart(new MobilePartType(manufacturer, MobilePartUnit.MICROPHONE, model + " Microphone"), randomGenerator.generateRandomBoolean(), serialNumberGenerator.generateSerialNumber()))
                .motherBoard(new MobilePart(new MobilePartType(manufacturer, MobilePartUnit.MOTHERBOARD, model + " MotherBoard"), randomGenerator.generateRandomBoolean(), serialNumberGenerator.generateSerialNumber()))
                .powerSwitch(new MobilePart(new MobilePartType(manufacturer, MobilePartUnit.POWER_SWITCH, model + " Power Switch"), randomGenerator.generateRandomBoolean(), serialNumberGenerator.generateSerialNumber()))
                .speaker(new MobilePart(new MobilePartType(manufacturer, MobilePartUnit.SPEAKER, model + " Speaker"), randomGenerator.generateRandomBoolean(), serialNumberGenerator.generateSerialNumber()))
                .microphone(new MobilePart(new MobilePartType(manufacturer, MobilePartUnit.MICROPHONE, model + " Microphone"), randomGenerator.generateRandomBoolean(), serialNumberGenerator.generateSerialNumber()))
                .volumeButtons(new MobilePart(new MobilePartType(manufacturer, MobilePartUnit.VOLUME_BUTTONS, model + " Volume Buttons"), randomGenerator.generateRandomBoolean(), serialNumberGenerator.generateSerialNumber()))
                .build();

        return  mobile;

    }

    private Mobile createHuaweiPhone() {
        Mobile mobile;
        Manufacturer manufacturer = Manufacturer.HUAWEI;
        String model = huaweiModels[randomGenerator.generateRandomPositiveInt(huaweiModels.length)];

        mobile = new Mobile.Builder(manufacturer, model)
                .display(new MobilePart(new MobilePartType(manufacturer, MobilePartUnit.DISPLAY, model + " Display"), randomGenerator.generateRandomBoolean(), serialNumberGenerator.generateSerialNumber()))
                .microphone(new MobilePart(new MobilePartType(manufacturer, MobilePartUnit.MICROPHONE, model + " Microphone"), randomGenerator.generateRandomBoolean(), serialNumberGenerator.generateSerialNumber()))
                .motherBoard(new MobilePart(new MobilePartType(manufacturer, MobilePartUnit.MOTHERBOARD, model + " MotherBoard"), randomGenerator.generateRandomBoolean(), serialNumberGenerator.generateSerialNumber()))
                .powerSwitch(new MobilePart(new MobilePartType(manufacturer, MobilePartUnit.POWER_SWITCH, model + " Power Switch"), randomGenerator.generateRandomBoolean(), serialNumberGenerator.generateSerialNumber()))
                .speaker(new MobilePart(new MobilePartType(manufacturer, MobilePartUnit.SPEAKER, model + " Speaker"), randomGenerator.generateRandomBoolean(), serialNumberGenerator.generateSerialNumber()))
                .microphone(new MobilePart(new MobilePartType(manufacturer, MobilePartUnit.MICROPHONE, model + " Microphone"), randomGenerator.generateRandomBoolean(), serialNumberGenerator.generateSerialNumber()))
                .volumeButtons(new MobilePart(new MobilePartType(manufacturer, MobilePartUnit.VOLUME_BUTTONS, model + " Volume Buttons"), randomGenerator.generateRandomBoolean(), serialNumberGenerator.generateSerialNumber()))
                .build();

        return  mobile;
    }

}
