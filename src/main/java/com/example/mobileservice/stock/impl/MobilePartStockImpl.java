package com.example.mobileservice.stock.impl;

import com.example.mobileservice.mobile.MobilePart;
import com.example.mobileservice.mobile.MobilePartType;
import com.example.mobileservice.stock.MobilePartStock;
import com.example.mobileservice.stock.MobilePartAvailableListener;
import com.example.mobileservice.supplier.MobilePartSupplier;
import com.example.mobileservice.supplier.Order;
import com.example.mobileservice.supplier.OrderStatus;
import com.example.mobileservice.supplier.OrderStatusChangedEvent;

import java.util.*;

public class MobilePartStockImpl implements MobilePartStock {

    private MobilePartSupplier mobilePartSupplier;
    private Map<MobilePartType, Deque<MobilePart>> mobilePartsByType = new HashMap<>();
    private int quantity = 5;

    public MobilePartStockImpl(MobilePartSupplier mobilePartSupplier) {
        this.mobilePartSupplier = mobilePartSupplier;
    }

    @Override
    public void getMobilePartAsync(MobilePartType partType,
                                   MobilePartAvailableListener partTypeAvailableListener) {

        if (isOrderSatisfiable(partType)) {
            MobilePart mobilePart = removePartFromStock(partType);
            partTypeAvailableListener.mobilePartIsAvailable(mobilePart);
        } else {
            orderFromSupplier(partType);
        }
    }

    private int getQuantityOfPart(MobilePartType partType) {
        return getPartsOfType(partType).size();
    }

    private Deque<MobilePart> getPartsOfType(MobilePartType partType) {
        Deque<MobilePart> mobileParts = mobilePartsByType.get(partType);
        if (mobileParts == null) {
            mobileParts = new LinkedList<>();
            mobilePartsByType.put(partType, mobileParts);
        }
        return mobileParts;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    private MobilePart removePartFromStock(MobilePartType partType) {
        final Deque<MobilePart> partsOfType = getPartsOfType(partType);
        return partsOfType.removeLast();
    }

    private void orderFromSupplier(MobilePartType partType) {
        mobilePartSupplier.orderPart(partType, quantity, this::handleOrderStatusChange);
    }

    private void handleOrderStatusChange(OrderStatusChangedEvent orderStatusChangedEvent) {
        final Order order = orderStatusChangedEvent.getObjectWithChangedStatus();
        if (order.getStatus() == OrderStatus.READY_FOR_SHIPMENT) {
            final List<MobilePart> arrivedParts = order.ship();
            addArrivedParts(order, arrivedParts);
        }
    }

    private void addArrivedParts(Order order, List<MobilePart> mobileParts) {
        getPartsOfType(order.getPartType()).addAll(mobileParts);
    }


    private boolean isOrderSatisfiable(MobilePartType partType) {
        return getQuantityOfPart(partType) > 0;
    }
}
