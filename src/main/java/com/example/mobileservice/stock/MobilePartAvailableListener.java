package com.example.mobileservice.stock;

import com.example.mobileservice.mobile.MobilePart;

public interface MobilePartAvailableListener {
    void mobilePartIsAvailable(MobilePart mobilePart);
}
