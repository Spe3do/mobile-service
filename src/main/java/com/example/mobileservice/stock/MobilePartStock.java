package com.example.mobileservice.stock;

import com.example.mobileservice.mobile.MobilePartType;

public interface MobilePartStock {

    void getMobilePartAsync(MobilePartType partType, MobilePartAvailableListener partTypeAvailableListener);
}
