package com.example.mobileservice.mobile;

/**
 *
 * @author vrg
 */
public class MobilePart {
    public MobilePartType type;
    public boolean failing;
    public String serialNumber;

    public MobilePart(MobilePartType type, boolean failing, String serialNumber) {
        this.type = type;
        this.failing = failing;
        this.serialNumber = serialNumber;
    }
}
