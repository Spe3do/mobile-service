package com.example.mobileservice.mobile;

import com.example.mobileservice.mobile.MobilePart;
import com.example.mobileservice.mobile.MobilePartType;

/**
 *
 * @author vrg
 */
public class ReplacedMobilePart extends MobilePart {

    public ReplacedMobilePart(MobilePartType type, String serialNumber) {
        super(type, false, serialNumber);
    }
    
}
