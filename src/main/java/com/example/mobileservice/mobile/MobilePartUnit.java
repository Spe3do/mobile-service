/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.example.mobileservice.mobile;

/**
 *
 * @author vrg
 */
public enum MobilePartUnit {
    DISPLAY,
    KEYBOARD,
    MOTHERBOARD,
    MICROPHONE,
    SPEAKER,
    VOLUME_BUTTONS,
    POWER_SWITCH
}
