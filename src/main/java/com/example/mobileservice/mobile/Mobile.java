package com.example.mobileservice.mobile;

import java.util.HashMap;

/**
 *
 * @author tmarton
 */
public class Mobile {
    private final Manufacturer manufacturer;
    private final String model;
    private final String otherInfo;
    private final HashMap<MobilePartUnit, MobilePart> mobileParts;

    private Mobile(Builder builder){
        manufacturer = builder.manufacturer;
        model = builder.model;
        otherInfo = builder.otherInfo;
        mobileParts = new HashMap<>();
        mobileParts.putAll(builder.mobileParts);

    }

    public Manufacturer getManufacturer() {
        return manufacturer;
    }

    public String getModel() {
        return model;
    }

    public MobilePart getDisplay() {
        return mobileParts.get(MobilePartUnit.DISPLAY);
    }

    public MobilePart getMotherBoard() {
        return mobileParts.get(MobilePartUnit.MOTHERBOARD);
    }

    public MobilePart getKeyboard() {
        return mobileParts.get(MobilePartUnit.KEYBOARD);
    }

    public MobilePart getMicrophone() {
        return mobileParts.get(MobilePartUnit.MICROPHONE);
    }

    public MobilePart getSpeaker() {
        return mobileParts.get(MobilePartUnit.SPEAKER);
    }

    public MobilePart getVolumeButtons() {
        return mobileParts.get(MobilePartUnit.VOLUME_BUTTONS);
    }

    public MobilePart getPowerSwitch() {
        return mobileParts.get(MobilePartUnit.POWER_SWITCH);
    }

    public String getOtherInfo() {
        return otherInfo;
    }

    public static class Builder {
        private Manufacturer manufacturer;
        private String model;
        private String otherInfo;

        private HashMap<MobilePartUnit, MobilePart> mobileParts;

        public Builder(Manufacturer manufacturer, String model){
            if(manufacturer == null){
                throw new IllegalArgumentException("Mobile manufacturer is required");
            }
            if(model == null) {
                throw new IllegalArgumentException("Mobile model is required");
            }
            if(model.isEmpty()){
                throw new IllegalArgumentException("Mobile model should not be empty");
            }

            this.manufacturer = manufacturer;
            this.model = model;
            mobileParts = new HashMap<>();
        }

        public Builder manufacturer(Manufacturer manufacturer){
            this.manufacturer = manufacturer;
            return this;
        }

        public Builder model(String model){
            this.model = model;
            return this;
        }

        public Builder display(MobilePart display){
            mobileParts.put(MobilePartUnit.DISPLAY, display);
            return this;
        }

        public Builder motherBoard(MobilePart motherBoard){
            mobileParts.put(MobilePartUnit.MOTHERBOARD, motherBoard);
            return this;
        }

        public Builder keyboard(MobilePart keyboard){
            mobileParts.put(MobilePartUnit.KEYBOARD, keyboard);
            return this;
        }

        public Builder microphone(MobilePart microphone){
            mobileParts.put(MobilePartUnit.MICROPHONE, microphone);
            return this;
        }

        public Builder speaker(MobilePart speaker){
            mobileParts.put(MobilePartUnit.SPEAKER, speaker);
            return this;
        }

        public Builder volumeButtons(MobilePart volumeButtons){
            mobileParts.put(MobilePartUnit.VOLUME_BUTTONS, volumeButtons);
            return this;
        }

        public Builder powerSwitch(MobilePart powerSwitch){
            mobileParts.put(MobilePartUnit.POWER_SWITCH, powerSwitch);
            return this;
        }

        public Builder otherInfo(String otherInfo){
            this.otherInfo = otherInfo;
            return this;
        }

        public Mobile build(){
            return new Mobile(this);
        }
    }

    @Override
    public String toString() {
        return "Mobile{" +
                "manufacturer=" + manufacturer +
                ", model='" + model + '\'' +
                ", otherInfo='" + otherInfo + '\'' +
                ", mobileParts=" + mobileParts +
                '}';
    }
}
