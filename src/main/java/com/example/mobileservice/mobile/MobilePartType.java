/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.example.mobileservice.mobile;

import java.util.Objects;

/**
 *
 * @author vrg
 */
public class MobilePartType {
    private Manufacturer manufacturer;
    private MobilePartUnit unit;
    private String description;

    private MobilePartType() {
    }

    public MobilePartType(Manufacturer manufacturer, MobilePartUnit unit, String description) {
        this.manufacturer = manufacturer;
        this.unit = unit;
        this.description = description;
    }

    public Manufacturer getManufacturer() {
        return manufacturer;
    }

    public MobilePartUnit getUnit() {
        return unit;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MobilePartType that = (MobilePartType) o;
        return manufacturer == that.manufacturer &&
                unit == that.unit &&
                Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(manufacturer, unit, description);
    }

    @Override
    public String toString() {
        return "MobilePartType{" +
                "manufacturer=" + manufacturer +
                ", unit=" + unit +
                ", description='" + description + '\'' +
                '}';
    }

    public static MobilePartTypeBuilder builder() {
        return new MobilePartTypeBuilder();
    }

    public static class MobilePartTypeBuilder {

        private MobilePartType partType = new MobilePartType();

        public MobilePartTypeBuilder manufacturer(Manufacturer manufacturer) {
            partType.manufacturer = manufacturer;
            return this;
        }

        public MobilePartTypeBuilder unit(MobilePartUnit unit) {
            partType.unit = unit;
            return this;
        }

        public MobilePartTypeBuilder description(MobilePartUnit unit) {
            partType.unit = unit;
            return this;
        }

        public MobilePartType build() {
            return partType;
        }
    }
}
