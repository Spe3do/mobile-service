package com.example.mobileservice;

import com.example.mobileservice.helper.*;
import com.example.mobileservice.service.MobilePhoneRepairServiceOriginal;
import com.example.mobileservice.stock.impl.MobilePartStockImpl;
import com.example.mobileservice.supplier.impl.MobilePartSupplierImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vrg
 */
public class Main {
    
    private MobilePhoneRepairServiceOriginal mobilePhoneRepairService;
    private MobilePartSupplierImpl mobilePartSupplier;
    private MobilePartStockImpl mobilePartStock;
    private final List<Client> clients = new ArrayList<>();
    private TaskScheduler scheduler = new TaskScheduler();
    private SerialNumberGenerator serialNumberGenerator = new IncrementalSerialNumberGenerator();
    
    public static void main(String[] args) {
        Main main = new Main();
        main.sendMobilesToService();
        main.startWorkAtMobileService();
    }

    public Main() {
        final IncrementalIdGenerator idGenerator = new IncrementalIdGenerator();
        mobilePartSupplier = new MobilePartSupplierImpl(scheduler, idGenerator, serialNumberGenerator);
        mobilePartStock = new MobilePartStockImpl(mobilePartSupplier);

        mobilePhoneRepairService = new MobilePhoneRepairServiceOriginal(mobilePartStock);
        for (int i = 0; i < 100; i++) {
            Client client = new Client(mobilePhoneRepairService);
            clients.add(client);
        }
    }
    
    public void sendMobilesToService() {
        for (Client client : clients) {
            SendMobileToServiceAndPollStatusTask task = new SendMobileToServiceAndPollStatusTask(client);
            new Thread(task).start();
        }
    }
    
    public void startWorkAtMobileService() {
      /*
        while(true) {
            mobilePhoneRepairService.pollSupplier();
            mobilePhoneRepairService.processWorksheets();
            mobilePhoneRepairService.orderParts();
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }*/
    }
    
}
