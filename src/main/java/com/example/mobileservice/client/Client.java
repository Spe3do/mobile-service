package com.example.mobileservice.client;

import com.example.mobileservice.mobile.Mobile;

public interface Client {

     void send(Mobile mobile);
     void setMobile(Mobile mobile);
     Mobile getMobile();

}
