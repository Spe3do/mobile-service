package com.example.mobileservice.client.impl;

import com.example.mobileservice.service.MobilePhoneRepairService;
import com.example.mobileservice.worksheet.imp.WorkSheet;
import com.example.mobileservice.client.Client;
import com.example.mobileservice.mobile.Mobile;

public class ClientImpl implements Client {
    private Mobile mobile;
    private WorkSheet workSheet;
    private MobilePhoneRepairService mobilePhoneRepairService;

    public ClientImpl(MobilePhoneRepairService mobilePhoneRepairService) {
        this.mobilePhoneRepairService = mobilePhoneRepairService;
    }

    public Mobile getMobile() {
        return mobile;
    }

    public void setMobile(Mobile mobile) {
        this.mobile = mobile;
    }

    @Override
    public void send(Mobile mobile){

    }

    @Override
    public String toString() {
        return "ClientImpl{" +
                "mobile=" + mobile +
                ", workSheet=" + workSheet +
                ", mobilePhoneRepairService=" + mobilePhoneRepairService +
                '}';
    }
}
