package com.example.mobileservice.worksheet;

import com.example.mobileservice.service.MobilePhoneRepairServiceOriginal;
import com.example.mobileservice.worksheet.imp.WorkSheet;
import com.example.mobileservice.listener.AbstractStatusChangedEvent;

public class WorksheetStatusChangedEvent extends AbstractStatusChangedEvent<MobilePhoneRepairServiceOriginal,WorkSheet> {

    public WorksheetStatusChangedEvent(MobilePhoneRepairServiceOriginal mobilePhoneRepairService, WorkSheet workSheet) {
        super(mobilePhoneRepairService, workSheet);
    }
}
