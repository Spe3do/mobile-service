package com.example.mobileservice.worksheet.imp;

import com.example.mobileservice.mobile.Mobile;
import com.example.mobileservice.mobile.MobilePart;
import com.example.mobileservice.mobile.ReplacedMobilePart;
import com.example.mobileservice.worksheet.InternalWorksheet;

import java.util.*;

/**
 *
 * @author vrg
 */
public class WorkSheet implements InternalWorksheet {

    private enum Status {
        RECEIVED,
        STARTED_TO_REPAIR,
        WAITING_FOR_PARTS,
        PARTS_ARRIVED,
        FINISHED,
        RETURNED
    }

    private Map<Status, Date> eventDates;
    private Mobile mobile;

    private ArrayList<MobilePart>partsToReplace;
    private ArrayList<ReplacedMobilePart>replacedParts;

    public WorkSheet(Mobile mobile, Date received) {
        eventDates = new HashMap<>();
        this.mobile = mobile;
        setWorkSheetStatus(Status.RECEIVED, received);
    }

    public void setWorkSheetStatus(Status status, Date date){
        Objects.requireNonNull(status, "The status must not be null");
        Objects.requireNonNull(date, "The status date must not be null");

        eventDates.put(status, date);
    }

    public Date getStatusDate(Status status){
        final Date date = eventDates.get(status);
        Objects.requireNonNull(date, "The required status: "+status.toString()+ " is not yet set.");

        return date;
    }

    public Mobile getMobile() {
        return mobile;
    }

    public void setMobile(Mobile mobile) {
        this.mobile = mobile;
    }

    public ArrayList<MobilePart> getPartsToReplace() {
        return partsToReplace;
    }

    public void setPartsToReplace(ArrayList<MobilePart> partsToReplace) {
        this.partsToReplace = partsToReplace;
    }

    public ArrayList<ReplacedMobilePart> getReplacedParts() {
        return replacedParts;
    }

    public void setReplacedParts(ArrayList<ReplacedMobilePart> replacedParts) {
        this.replacedParts = replacedParts;
    }

    public Date getReceivedOn() {
        return getStatusDate(Status.RECEIVED);
    }

    public void setReceivedOn(Date receivedOn) {
        setWorkSheetStatus(Status.RECEIVED, receivedOn);
    }

    public Date getPartsOrderedOn() {
        return getStatusDate(Status.WAITING_FOR_PARTS);
    }

    public void setPartsOrderedOn(Date partsOrderedOn) {
        setWorkSheetStatus(Status.WAITING_FOR_PARTS, partsOrderedOn);
    }

    public Date getPartsArrivedOn() {
        return getStatusDate(Status.PARTS_ARRIVED);
    }

    public void setPartsArrivedOn(Date partsArrivedOn) {
        setWorkSheetStatus(Status.PARTS_ARRIVED, partsArrivedOn);
    }

    public Date getStartedToRepairOn() {
        return getStatusDate(Status.STARTED_TO_REPAIR);
    }

    public void setStartedToRepairOn(Date startedToRepairOn) {
        setWorkSheetStatus(Status.STARTED_TO_REPAIR,startedToRepairOn);
    }

    public Date getFinishedToRepairOn() {
        return getStatusDate(Status.FINISHED);
    }

    public void setFinishedToRepairOn(Date finishedToRepairOn) {
        setWorkSheetStatus(Status.FINISHED, finishedToRepairOn);
    }

    public Date getGivenBackOn() {
        return getStatusDate(Status.RETURNED);
    }

    public void setGivenBackOn(Date givenBackOn) {
        setWorkSheetStatus(Status.RETURNED, givenBackOn);
    }
}
