package com.example.mobileservice;

import com.example.mobileservice.mobile.*;
import com.example.mobileservice.service.MobilePhoneRepairServiceOriginal;
import com.example.mobileservice.worksheet.imp.WorkSheet;

import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vrg
 */
public final class Client {


    private final static AtomicLong counter = new AtomicLong();
    private final static AtomicLong counter2 = new AtomicLong();

    private final long id = counter.incrementAndGet();

    private MobilePhoneRepairServiceOriginal service;
    private Random random = new Random();
    private static final Manufacturer[] manufacturers = Manufacturer.values();
    private static final String[] samsungModels = {"Galaxy S3", "Galaxy S4", "Galaxy S5", "Galaxy S6", "Galaxy S7", "Galaxy Note"};
    private static final String[] htcModels = {"One", "Desire 610", "One Mini"};
    private static final String[] huaweiModels = {"Ascend P7", "Ascend P2", "Ascend G630"};
    private static final String[] appleModels = {"iPhone 4s", "iPhone 5s", "iPhone5c", "iPhone 6", "iPhone 6s", "iPhone 7", "iPhone 8", "iPhone X"};

    public Client(MobilePhoneRepairServiceOriginal service) {
        this.service = service;

    }

    private String generateSerial() {
        return String.valueOf(1000000L + counter2.incrementAndGet());
    }



}
