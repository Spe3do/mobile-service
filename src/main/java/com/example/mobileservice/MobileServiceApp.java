package com.example.mobileservice;


import com.example.mobileservice.helper.IncrementalIdGenerator;
import com.example.mobileservice.helper.IncrementalSerialNumberGenerator;
import com.example.mobileservice.helper.SerialNumberGenerator;
import com.example.mobileservice.helper.TaskScheduler;
import com.example.mobileservice.service.MobilePhoneRepairServiceOriginal;
import com.example.mobileservice.spring.AppConfig;
import com.example.mobileservice.stock.MobilePartStock;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MobileServiceApp {
    private static AnnotationConfigApplicationContext context;

    private MobilePhoneRepairServiceOriginal mobilePhoneRepairService;
    private MobilePartStock mobilePartStock;
    private final List<Client> clients = new ArrayList<>();
    private TaskScheduler scheduler = new TaskScheduler();
    private SerialNumberGenerator serialNumberGenerator = new IncrementalSerialNumberGenerator();


    public static void main(String[] args) {
        context = new AnnotationConfigApplicationContext(AppConfig.class);

        MobileServiceApp mobileServiceApp = new MobileServiceApp();
        mobileServiceApp.sendMobilesToService();
        //mobileServiceApp.startWorkAtMobileService();

    }

    private MobileServiceApp() {
        final IncrementalIdGenerator idGenerator = context.getBean(IncrementalIdGenerator.class);
        mobilePartStock = context.getBean(MobilePartStock.class);

        mobilePhoneRepairService = new MobilePhoneRepairServiceOriginal(mobilePartStock);
        for (int i = 0; i < 100; i++) {
            Client client = new Client(mobilePhoneRepairService);
            clients.add(client);
        }
    }

    private void sendMobilesToService() {
        for (Client client : clients) {
            SendMobileToServiceAndPollStatusTask task = new SendMobileToServiceAndPollStatusTask(client);
            new Thread(task).start();
        }
    }

    /*private void startWorkAtMobileService() {
        while(true) {
            mobilePhoneRepairService.pollSupplier();
            mobilePhoneRepairService.processWorksheets();
            mobilePhoneRepairService.orderParts();
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }*/



}
