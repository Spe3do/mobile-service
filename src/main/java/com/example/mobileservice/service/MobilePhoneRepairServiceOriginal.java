package com.example.mobileservice.service;

import com.example.mobileservice.worksheet.imp.WorkSheet;
import com.example.mobileservice.mobile.Mobile;
import com.example.mobileservice.mobile.MobilePart;
import com.example.mobileservice.mobile.MobilePartType;
import com.example.mobileservice.mobile.ReplacedMobilePart;
import com.example.mobileservice.stock.MobilePartStock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *
 * @author vrg
 */
public class MobilePhoneRepairServiceOriginal {

    private List<WorkSheet>worksheetsToProcess = new ArrayList<>();
    private Map<MobilePartType,Long>orderIdByPartType = new HashMap<>();
    private MobilePartStock mobilePartStock;
    private MobileRepairShop mobileRepairShop;

    public MobilePhoneRepairServiceOriginal(MobilePartStock mobilePartStock) {
        this.mobilePartStock = mobilePartStock;
    }

/*
    public synchronized WorkSheet sendIn(Mobile mobile) {
        WorkSheet worksheet = createWorkSheet(mobile);
        collectFailingParts(worksheet);
        worksheetsToProcess.add(worksheet);
        return worksheet;
    }
*/

    /*public synchronized boolean replaceAllPossibleFailingParts(WorkSheet ws) {
        if (ws.partsToReplace == null) {
            //nothing to replace
            ws.status = WorkSheet.Status.FINISHED;
            return true;
        }
        for (Iterator<MobilePart> it = ws.partsToReplace.iterator(); it.hasNext();) {
            MobilePart mobilePartToReplace = it.next();
            MobilePartType type = mobilePartToReplace.type;
            Integer inStockQuantity = stock.get(type);
            if (inStockQuantity != null && inStockQuantity > 0) {
                inStockQuantity = inStockQuantity - 1;
                stock.put(type, inStockQuantity);
                if (ws.replacedParts == null) {
                    ws.replacedParts = new ArrayList<>();
                }
                ws.replacedParts.add(new ReplacedMobilePart(type, mobilePartToReplace.serialNumber + 1000));
                it.remove();
            } else {
                addOrderForMissingPart(type);
                ws.status = WorkSheet.Status.WAITING_FOR_PARTS;
            }
        }

        if (ws.partsToReplace.isEmpty()) {
            ws.status = WorkSheet.Status.FINISHED;
            return true;
        } else {
            return false;
        }
    }

    public synchronized void addOrderForMissingPart(MobilePartType type) {
        if (!orderIdByPartType.containsKey(type)) {
            orderIdByPartType.put(type, null);
        }
    }

    public synchronized void processWorksheets() {

        for (Iterator<WorkSheet> it = worksheetsToProcess.iterator(); it.hasNext();) {
            WorkSheet workSheet = it.next();
            boolean finished = replaceAllPossibleFailingParts(workSheet);
            if (finished) {
                it.remove();
            }
        }
    }

    public synchronized boolean replacePart(WorkSheet ws, MobilePart mobilePart) {
        MobilePartType type = mobilePart.type;
        Integer inStockQuantity = stock.get(type);
        if (inStockQuantity != null && inStockQuantity > 0) {
            inStockQuantity = inStockQuantity - 1;
            stock.put(type, inStockQuantity);
            if (ws.replacedParts == null) {
                ws.replacedParts = new ArrayList<>();
            }
            ws.replacedParts.add(new ReplacedMobilePart(type, mobilePart.serialNumber));
            ws.partsToReplace.remove(mobilePart);
            return true;
        } else {
            return false;
        }

    }

    public synchronized WorkSheet createWorkSheet(Mobile mobile) {
        WorkSheet ws = new WorkSheet();
        ws.mobile = mobile;
        ws.receivedOn = new Date();
        ws.status = WorkSheet.Status.RECEIVED;
        return ws;
    }

    public synchronized void collectFailingParts(WorkSheet sheet) {

        sheet.status = WorkSheet.Status.STARTED_TO_REPAIR;

        Mobile mobile = sheet.mobile;

        if (mobile.getDisplay().failing) {
            addPartToReplace(sheet, mobile.getDisplay());
        }
        if (mobile.getKeyboard() != null && mobile.getKeyboard().failing) {
            addPartToReplace(sheet, mobile.getKeyboard());
        }
        if (mobile.getMicrophone().failing) {
            addPartToReplace(sheet, mobile.getMicrophone());
        }
        if (mobile.getMotherBoard().failing) {
            addPartToReplace(sheet, mobile.getMotherBoard());
        }
        if (mobile.getPowerSwitch().failing) {
            addPartToReplace(sheet, mobile.getPowerSwitch());
        }
        if (mobile.getSpeaker().failing) {
            addPartToReplace(sheet, mobile.getSpeaker());
        }
        if (mobile.getVolumeButtons().failing) {
            addPartToReplace(sheet, mobile.getVolumeButtons());
        }
    }

    public synchronized void addPartToReplace(WorkSheet sheet, MobilePart mobilePart) {
        if (sheet.partsToReplace == null) {
            sheet.partsToReplace = new ArrayList<>();
        }
        sheet.partsToReplace.add(mobilePart);
    }

    public synchronized void orderParts() {
        if (!orderIdByPartType.isEmpty()) {
            for (Map.Entry<MobilePartType, Long> entry : orderIdByPartType.entrySet()) {
                Long orderId = entry.getValue();
                if (orderId == null) {
//                    orderId = mobilePartSupplier.orderPart(entry.getKey(), 5);
                    entry.setValue(orderId);
                }
            }
        }
    }
    
    public synchronized void pollSupplier() {
        if (!orderIdByPartType.isEmpty()) {
            List<Long> orderIds = new ArrayList<>(orderIdByPartType.values());
            for (Long orderId : orderIds) {
                if (orderId != null) {
//                    if (mobilePartSupplier.isReadyForShipment(orderId)) {
//                        InternalOrder shippedOrder = mobilePartSupplier.shipOrder(orderId);
//                        stock.put(shippedOrder.getPartType(), shippedOrder.getQuantity());
//                        orderIdByPartType.remove(shippedOrder.getPartType());
//                    }
                }
            }
        }
    }*/
}
