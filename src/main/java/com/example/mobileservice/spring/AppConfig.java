package com.example.mobileservice.spring;

import com.example.mobileservice.client.Client;
import com.example.mobileservice.client.impl.ClientImpl;
import com.example.mobileservice.helper.*;
import com.example.mobileservice.service.MobilePhoneRepairService;
import com.example.mobileservice.service.MobilePhoneRepairServiceOriginal;
import com.example.mobileservice.stock.MobilePartStock;
import com.example.mobileservice.stock.impl.MobilePartStockImpl;
import com.example.mobileservice.supplier.MobilePartSupplier;
import com.example.mobileservice.supplier.impl.MobilePartSupplierImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Bean
    IdGenerator idGenerator(){
        return new IncrementalIdGenerator();
    }

    @Bean
    SerialNumberGenerator serialNumberGenerator(){
        return new IncrementalSerialNumberGenerator();
    }

    @Bean
    TaskScheduler taskScheduler(){
        return new TaskScheduler();
    }

    @Bean
    MobilePartSupplier mobilePartSupplier(TaskScheduler scheduler, IdGenerator idGenerator, SerialNumberGenerator serialNumberGenerator){
        return new MobilePartSupplierImpl(scheduler, idGenerator, serialNumberGenerator);
    }

    @Bean
    MobilePartStock mobilePartStock(MobilePartSupplier mobilePartSupplier){
        return new MobilePartStockImpl(mobilePartSupplier);
    }

    @Bean
    MobilePhoneRepairService mobilePhoneRepairService(MobilePartStock mobilePartStock){
        return new MobilePhoneRepairService(mobilePartStock);
    }

    @Bean
    Client client(MobilePhoneRepairService mobilePhoneRepairService){
        return new ClientImpl(mobilePhoneRepairService);
    }
}
