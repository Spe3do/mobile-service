package com.example.mobileservice.supplier;

public enum OrderStatus {

    ORDERED,
    READY_FOR_SHIPMENT,
    SHIPPED
}
