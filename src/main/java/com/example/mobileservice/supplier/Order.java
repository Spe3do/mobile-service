package com.example.mobileservice.supplier;

import com.example.mobileservice.mobile.MobilePart;
import com.example.mobileservice.mobile.MobilePartType;

import java.util.List;

public interface Order {

    long getId();

    OrderStatus getStatus();

    MobilePartType getPartType();

    int getQuantity();

    List<MobilePart> ship();

    @Override
    String toString();
}
