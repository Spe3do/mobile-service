package com.example.mobileservice.supplier.impl;

import com.example.mobileservice.mobile.MobilePartType;
import com.example.mobileservice.helper.IdGenerator;
import com.example.mobileservice.helper.SerialNumberGenerator;
import com.example.mobileservice.helper.TaskScheduler;
import com.example.mobileservice.listener.StatusChangeListener;
import com.example.mobileservice.supplier.MobilePartSupplier;
import com.example.mobileservice.supplier.Order;
import com.example.mobileservice.supplier.OrderStatusChangedEvent;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author vrg
 */
public class MobilePartSupplierImpl implements MobilePartSupplier {

    private TaskScheduler scheduler;
    private IdGenerator idGenerator;
    private SerialNumberGenerator serialNumberGenerator;

    public MobilePartSupplierImpl(TaskScheduler scheduler, IdGenerator idGenerator, SerialNumberGenerator serialNumberGenerator) {
        this.scheduler = scheduler;
        this.idGenerator = idGenerator;
        this.serialNumberGenerator = serialNumberGenerator;
    }

    /**
     * Orders parts of the specified type and the specified quantity.
     *
     * @param mobilePartType
     * @param quantity
     * @return orderId
     */
    public Order orderPart(MobilePartType mobilePartType, int quantity, StatusChangeListener<OrderStatusChangedEvent> orderStatusListener) {
        Objects.requireNonNull(mobilePartType, "mobilePartType cannot be null");
        if (quantity < 1) {
            throw new IllegalArgumentException("quantity should be at least 1");
        }
        InternalOrder order = new InternalOrder(idGenerator.generateId(), serialNumberGenerator, orderStatusListener);
        order.setPartType(mobilePartType);
        order.setQuantity(quantity);
        System.out.println("New order: " + order);

        Runnable satisfyOrder = () -> order.manufactureOrderedParts();

        scheduler.scheduleTaskToRandomTime(satisfyOrder, 1, 10, TimeUnit.SECONDS);
        return order;
    }

}
