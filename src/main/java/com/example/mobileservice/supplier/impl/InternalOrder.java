package com.example.mobileservice.supplier.impl;

import com.example.mobileservice.mobile.MobilePart;
import com.example.mobileservice.mobile.MobilePartType;
import com.example.mobileservice.helper.SerialNumberGenerator;
import com.example.mobileservice.listener.StatusChangeListener;
import com.example.mobileservice.supplier.Order;
import com.example.mobileservice.supplier.OrderStatus;
import com.example.mobileservice.supplier.OrderStatusChangedEvent;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author vrg
 */
public class InternalOrder implements Order {

    private List<StatusChangeListener<OrderStatusChangedEvent>>orderStatusChangeListeners = new ArrayList<>();
    private StatusChangeListener<OrderStatusChangedEvent>statusChangeListener;

    private long id;
    private MobilePartType partType;
    private int quantity;
    private OrderStatus orderStatus;
    private SerialNumberGenerator serialNumberGenerator;
    private List<MobilePart>manufacturedParts;

    public InternalOrder(long id, SerialNumberGenerator serialNumberGenerator, StatusChangeListener<OrderStatusChangedEvent>statusChangeListener) {
        this.id = id;
        this.serialNumberGenerator = serialNumberGenerator;
        this.orderStatus = OrderStatus.ORDERED;
        this.statusChangeListener = statusChangeListener;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public OrderStatus getStatus() {
        return orderStatus;
    }

    @Override
    public MobilePartType getPartType() {
        return partType;
    }

    public void setPartType(MobilePartType partType) {
        this.partType = partType;
    }

    @Override
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setOrderStatus(OrderStatus newOrderStatus) {
        switch (newOrderStatus) {
            case READY_FOR_SHIPMENT:
                if (orderStatus != OrderStatus.ORDERED) {
                    throw new IllegalStateException("Illegal state transition: " + orderStatus + " -> " + newOrderStatus);
                }
                break;
            case SHIPPED:
                if (orderStatus != OrderStatus.READY_FOR_SHIPMENT) {
                    throw new IllegalStateException("Order should be in READY_FOR_SHIPMENT state in order to be shipped successfully.");
                }
                break;
            default:
                throw new IllegalStateException("Illegal state transition: " + orderStatus + " -> " + newOrderStatus);
        }
        orderStatus = newOrderStatus;
        fireStatusChangedEvent();
    }

    public void manufactureOrderedParts() {
        if (orderStatus != OrderStatus.ORDERED) {
            throw new IllegalStateException("Parts were already manufactured! Current state is " + orderStatus);
        }
        manufacturedParts = createMobileParts();
        setOrderStatus(OrderStatus.READY_FOR_SHIPMENT);
    }

    @Override
    public List<MobilePart> ship() {
        setOrderStatus(OrderStatus.SHIPPED);
        return manufacturedParts;
    }

    private List<MobilePart> createMobileParts() {
        List<MobilePart>shippedParts = new ArrayList<>();
        for (int i = 0; i < quantity; i++) {
            shippedParts.add(new MobilePart(partType, false, serialNumberGenerator.generateSerialNumber()));
        }
        return shippedParts;
    }

    @Override
    public String toString() {
        return "InternalOrder{" + "id=" + id + ", partType=" + partType + ", quantity=" + quantity + ", orderStatus=" + orderStatus + '}';
    }

    protected void fireStatusChangedEvent() {
        final OrderStatusChangedEvent event = new OrderStatusChangedEvent(this, this);
        statusChangeListener.statusChanged(event);
    }
}
