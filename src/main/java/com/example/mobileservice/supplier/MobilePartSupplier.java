package com.example.mobileservice.supplier;

import com.example.mobileservice.mobile.MobilePartType;
import com.example.mobileservice.listener.StatusChangeListener;

public interface MobilePartSupplier {
    Order orderPart(MobilePartType mobilePartType, int quantity, StatusChangeListener<OrderStatusChangedEvent> orderStatusListener);
}
